# Front


A simple user login app built using Vue.js. This app allows access to secure portion of website only after the user has authenticated.

## Prerequisites

- Vue.js cli installed. To install cli run this command `npm install -g @vue/cli`. To check if the cli is already installed run the following command `vue --version`.

## Running Project 

### Installer Npm 


#### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run start
```

#### Compiles and minifies for production
```
npm run build
```
### Installer Yarn 


#### Project setup
```
yarn install
```

#### Compiles and hot-reloads for development
```
yarn start
```

#### Compiles and minifies for production
```
yarn build
```